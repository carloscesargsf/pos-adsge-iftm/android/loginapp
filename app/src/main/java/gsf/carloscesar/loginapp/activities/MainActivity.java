package gsf.carloscesar.loginapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import gsf.carloscesar.loginapp.R;
import gsf.carloscesar.loginapp.entities.CredentialsDTO;
import gsf.carloscesar.loginapp.entities.TokenDTO;
import gsf.carloscesar.loginapp.services.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView userText;
    TextView passwordText;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userText = findViewById(R.id.login_form_email);
        passwordText = findViewById(R.id.login_form_password);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void login(View view) {
        String user = userText.getText().toString();
        String password = passwordText.getText().toString();

        CredentialsDTO credentialsDTO = new CredentialsDTO(user, password);
        RetrofitService.getService().authenticate(credentialsDTO).enqueue(new Callback<TokenDTO>() {
            @Override
            public void onResponse(Call<TokenDTO> call, Response<TokenDTO> response) {
                Log.d(TAG, "onResponse" + response.body().getToken());
                SharedPreferences sp = getSharedPreferences("user", 0);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("token", response.body().getToken());
                editor.apply();
                startActivity(new Intent(MainActivity.this, UserInsertActivity.class));
            }

            @Override
            public void onFailure(Call<TokenDTO> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(MainActivity.this, "Erro. Tente novamente!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
