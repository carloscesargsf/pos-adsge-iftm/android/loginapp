package gsf.carloscesar.loginapp.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import gsf.carloscesar.loginapp.R;
import gsf.carloscesar.loginapp.entities.UserDTO;
import gsf.carloscesar.loginapp.entities.UserInsertDTO;
import gsf.carloscesar.loginapp.services.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserInsertActivity extends AppCompatActivity {

    private final String TAG = getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_insert_user);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void Save(View view) {
        TextView textViewName = (TextView) findViewById(R.id.et_name);
        TextView textViewEmail = (TextView) findViewById(R.id.et_email);
        TextView textViewPhone = (TextView) findViewById(R.id.et_phone);
        TextView textViewPassword = (TextView) findViewById(R.id.et_password);

        UserInsertDTO userInsertDTO = new UserInsertDTO();
        userInsertDTO.setName(textViewName.getText().toString());
        userInsertDTO.setEmail(textViewEmail.getText().toString());
        userInsertDTO.setPhone(textViewPhone.getText().toString());
        userInsertDTO.setPassword(textViewPassword.getText().toString());

        RetrofitService.getService().insertUser(userInsertDTO).enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                Log.d(TAG, "" + R.string.on_response + response.body().getId());
                Toast.makeText(UserInsertActivity.this, R.string.registro_inserido_com_sucesso, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d(TAG, "" + R.string.on_failure + t.getMessage());
            }
        });
    }

}
