package gsf.carloscesar.loginapp.entities;

public class TokenDTO {

    private String email;

    private String token;

    public TokenDTO() {
    }

    public TokenDTO(String email, String token) {
        setEmail(email);
        setToken(token);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
