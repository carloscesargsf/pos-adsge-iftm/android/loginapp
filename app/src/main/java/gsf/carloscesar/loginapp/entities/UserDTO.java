package gsf.carloscesar.loginapp.entities;

public class UserDTO {

    public Long id;

    public String name;

    public String email;

    public String phone;

    public UserDTO() {
    }

    public UserDTO(UserInsertDTO userInsertDTO) {
        setId(userInsertDTO.getId());
        setName(userInsertDTO.getName());
        setEmail(userInsertDTO.getEmail());
        setPhone(userInsertDTO.getPhone());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
