package gsf.carloscesar.loginapp.services;

import gsf.carloscesar.loginapp.entities.CredentialsDTO;
import gsf.carloscesar.loginapp.entities.TokenDTO;
import gsf.carloscesar.loginapp.entities.UserDTO;
import gsf.carloscesar.loginapp.entities.UserInsertDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiEndPoint {

    @POST("auth/login")
    Call<TokenDTO> authenticate(@Body CredentialsDTO credentialsDTO);

    @POST("users")
    Call<UserDTO> insertUser(@Body UserInsertDTO userInsertDTO);

}
